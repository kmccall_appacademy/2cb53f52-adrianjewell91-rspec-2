def measure(num=1)
  arr= []
  #can also use while loop: i=0  while i<num yield i+=1 end #then do average.
  num.times do
    first = Time.now
    yield
    second = Time.now
    arr << second - first
  end

  arr.reduce(:+)/arr.length #this returns the total time.
end

#Need to return the avg running time across all trials
#I need it to compute the average time
