def reverser
   yield.split(' ').map(&:reverse).join(" ") #Thank you map function
end

def adder(num=1)
  yield + num
end

def repeater(num=1)
  num.times {yield}
end
